# Compilation on Taurus

```
module load imkl
gcc matmul-0.c -o matmul -Wl,--no-as-needed -lmkl_intel_ilp64 -lmkl_sequential -lmkl_core -lpthread -lm -ldl
```

# Execution

```
./matmul 2048
```
