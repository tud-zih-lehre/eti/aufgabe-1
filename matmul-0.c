#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/time.h>                // for gettimeofday()
#include <math.h>
#include "mkl.h"

#ifndef REP
#define REP 10
#endif

#ifndef N
#define N 1024
#endif

/** performs the matrix multiplication of two matrices and returns the runtime
 * @param n size of the matrices (will be n*n)
 * @param return runtime of the matric mutliplication
 */
double matmul(int n)
{
    double *a, *b, *c;
    struct timeval t1, t2;
    double elapsedTime;

    a = (double*)malloc(n*n*sizeof(double));
    b = (double*)malloc(n*n*sizeof(double));
    c = (double*)malloc(n*n*sizeof(double));

    //initialize a,b and c
    for (int i=0; i < n*n; i++)
    {
        a[i] = b[i] = i * 1.0 + 1 ;
        c[i] = 0;
    }

    double alpha = 1.0;
    double beta = 0.0;

    gettimeofday(&t1, NULL);
    //matmul
    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, n, n, n, alpha, a, n, b, n, beta, c, n);
    gettimeofday(&t2, NULL);
    
    elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
    elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms
    elapsedTime = elapsedTime * 1e-3; //back to seconds

    double sum_cpu = 0;
    for (int i=0; i<n*n; i++)
    {
        sum_cpu += c[i];
    }        
    
    printf("sum mat %f\n",sum_cpu);

    free(a);
    free(b);
    free(c);

    return elapsedTime;
}

double matmul_manual(int n)
{
    double *a, *b, *c, *c_tmp;
    struct timeval t1, t2;
    double elapsedTime;

    a = (double*)malloc(n*n*sizeof(double));
    b = (double*)malloc(n*n*sizeof(double));
    c = (double*)malloc(n*n*sizeof(double));

    //initialize a,b and c
    for (int i=0; i < n*n; i++)
    {
        a[i] = b[i] = i * 1.0 + 1 ;
        c[i] = 0;
    }

    gettimeofday(&t1, NULL);

    for (int i = 0; i < n; ++i )
    {
        for (int k = 0; k < n; ++k )
        {
            for (int j = 0; j < n; ++j )
            {
                c[ i*n + j ] += a[ i*n + k ] * b[ k*n + j ];
            }
        }
    }
    gettimeofday(&t2, NULL);
    //printf("end calculation\n");
    
    elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
    elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms
    elapsedTime = elapsedTime * 1e-3;

    double sum_cpu = 0;
    for (int i=0; i<n*n; i++)
    {
        sum_cpu += c[i];
    }        
    
    printf("sum mat %f\n",sum_cpu);
    
    free(a);
    free(b);
    free(c);

    return elapsedTime;
}

int main(int argc, char *argv[]) {

    double time, avg_time, flop, gflop, gflops;
    int n;
    if (argc == 2)
    {
        n = atoi(argv[1]);
    } else {
        n = N;
    }

    printf("computing \"matmul\" for %d elements\n", n); 

    time = 0;
    for(int i = 0; i < REP; i++)
    {
      time += matmul(n);
    } 
    flop = 2.0*n*n*n;
    gflop = flop*1e-9;
    avg_time = time/REP;
    gflops = gflop/avg_time;

    printf("average perfomrance of \"matmul\" %f GFLOPS\n", gflops); 


    printf("computing \"matmul_manual\" for %d elements\n", n); 

    time = 0;
    for(int i = 0; i < REP; i++)
    {
      time += matmul_manual(n);
    } 
    flop = 2.0*n*n*n;
    gflop = flop*1e-9;
    avg_time = time/REP;
    gflops = gflop/avg_time;

    printf("average perfomrance of \"matmul_manual\" %f GFLOPS\n", gflops); 

    return 0;
}
